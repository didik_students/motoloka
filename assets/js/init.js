$('nav .search-form').addClass('d-none');

$('.search').on('click', function () {
    $('nav .search-form').removeClass('d-none');
    $('.search').addClass('d-none');
});
$('nav .search-form i').on('click', function () {
    $('nav .search-form').addClass('d-none');
    $('.search').removeClass('d-none');
});

// SweetAllert2
/* Swal Success */
function successPengajuan() {
    swal.fire("Transaksi Success", "Terima kasih telah melakukan pembelian melalui motoloka. Selanjutnya silahkan melakukan pembayaran, sesuai dengan total tagihan yang tertera pada detail pembayaran.", "success");
};
function successKirimBukti() {
    swal.fire("Konfirmasi Success", "Terima kasih telah melakukan konfirmasi. Anda akan segera dihubungi oleh Sales Kami.", "success");
};
function successAddReview() {
    swal.fire("Terimakasih", "Review Anda telah terkirim", "success");
};