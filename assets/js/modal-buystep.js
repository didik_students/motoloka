$(document).ready(function() {

    // Dropify
    $('.dropify').dropify();

    // Setting Modal
    $(".modal-buy .modal-header").hide();
    $(".buyStep").hide();
    /* Button Go To Step */
    $("#goStep").on("click", function () {
        $(".confirm").hide();
        $(".buyStep").show();
        $(".close").hide();
        $(".modal-header").show();
        $(".modal-dialog").addClass("modal-lg");
        $(".buyStep").find("#step2-tab").addClass("active");
    });
    /* Button Sales Profil */
    $("#backConfirm").on("click", function () {
        $(".confirm").show();
        $(".buyStep").hide();
        $(".close").show();
        $(".modal-header").hide();
        $(".modal-dialog").removeClass("modal-lg");
    });
    /* Button Next Step */
    $("a.btn").on('click', function(){
        if($("a.btn").hasClass("active")) {
            $("a.btn").removeClass("active");
            active_tab = $(this).attr('id');
            current_tab = $(".buyStep").find('#'+active_tab+'').attr("id");
            $(".buyStep").find(".nav-link").removeClass("active");
            if(active_tab == current_tab) {
                $(".buyStep").find('#'+active_tab+'').addClass("active");
            }
            else {
                $(".buyStep").find(".nav-link").removeClass("active");
            }
        }
        else {
            active_tab = $(this).attr('id');
            current_tab = $(".buyStep").find('#'+active_tab+'').attr("id");
            if(active_tab == current_tab) {
                $(".buyStep").find(".nav-link").removeClass("active");
                $(".buyStep").find('#'+active_tab+'').addClass("active");
            }
            else {
                $(".buyStep").find(".nav-link").removeClass("active");
            }
        }
    });
    /* Choose Color Bike */
    var blue = "blue";
    var red = "red";
    var black = "black";
    $("button.bike-color."+blue).on("click", function () {
        $(".bike-img-group img").attr("src", "assets/images/product/beat-esp-"+blue+".png");
        $("button.bike-color").removeClass("active");
        $(this).addClass("active");
    });
    $("button.bike-color."+red).on("click", function () {
        $(".bike-img-group img").attr("src", "assets/images/product/beat-esp-"+red+".png");
        $("button.bike-color").removeClass("active");
        $(this).addClass("active");
    });
    $("button.bike-color."+black).on("click", function () {
        $(".bike-img-group img").attr("src", "assets/images/product/beat-esp-"+black+".png");
        $("button.bike-color").removeClass("active");
        $(this).addClass("active");
    });
});